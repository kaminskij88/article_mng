# Article manager app

## Description

Simple application to menage articles

## Used technologies

- React
- Redux
- Hooks
- Axios
- React-bootstrap
- SASS
- Atomic design 

Application is supported by Firebase API and Database.