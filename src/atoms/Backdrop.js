import "../molecules/Modal.scss";

const Backdrop = (props) => {
  return (
    <div
      data-testid="backdrop"
      className="backdrop"
      onClick={props.onClose}
    ></div>
  );
};

export default Backdrop;
