import { Table, Button, Row, Col, Spinner } from "react-bootstrap";
import "./StyledTable.scss";
import { Fragment } from "react";
import React from "react";

const StyledTable = ({
  data,
  tableHeaders,
  deleteRecord,
  isLoading,
  showRecord,
  editRecord,
}) => {
  const deleteHandler = (id) => {
    deleteRecord(id);
  };

  const showHandler = (id) => {
    showRecord(id);
  };

  const editHandler = (article) => {
    editRecord(article);
  };

  return (
    <Fragment>
      {isLoading ? (
        <Row>
          <Col className="text-center mt-2 mb-2">
            <Spinner animation="border" variant="primary" />
          </Col>
        </Row>
      ) : (
        <Table bordered>
          <thead>
            <tr>
              {tableHeaders.map((header) => (
                <th className="headers" key={header.id}>
                  {header.name}
                </th>
              ))}
            </tr>
          </thead>

          <tbody>
            {data.reverse().map((data) => (
              <tr key={data.key} className="text-content">
                <td>{data.value.title}</td>
                <td>{data.value.description}</td>
                <td>{data.value.publish_date}</td>
                <td>
                  <Row className="justify-content-md-center">
                    <Col md="auto">
                      <Button
                        onClick={showHandler.bind(null, data.key)}
                        variant="primary"
                      >
                        Show
                      </Button>
                    </Col>
                    <Col md="auto">
                      <Button
                        onClick={editHandler.bind(null, data)}
                        variant="info"
                      >
                        Edit
                      </Button>
                    </Col>
                    <Col md="auto">
                      <Button
                        onClick={deleteHandler.bind(null, data.key)}
                        variant="danger"
                      >
                        Delete
                      </Button>
                    </Col>
                  </Row>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      )}
    </Fragment>
  );
};

export default React.memo(StyledTable);
