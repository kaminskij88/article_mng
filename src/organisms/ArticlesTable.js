import StyledTable from "../molecules/StyledTable";
import { Container, Col, Row } from "react-bootstrap";
import React from "react";

const ArticlesTable = ({
  articles,
  deleteHandler,
  isLoading,
  showHandler,
  editHandler,
}) => {
  const tableHeaders = [
    { id: 1, name: "Title" },
    { id: 2, name: "Description" },
    { id: 3, name: "Publish Date" },
    { id: 4, name: "Action" },
  ];

  const deleteArticle = (id) => {
    deleteHandler(id);
  };

  return (
    <Container className="form-container">
      <Row className="justify-content-md-center">
        <Col md={12}>
          <StyledTable
            data={articles}
            tableHeaders={tableHeaders}
            deleteRecord={deleteArticle}
            isLoading={isLoading}
            showRecord={showHandler}
            editRecord={editHandler}
          />
        </Col>
      </Row>
    </Container>
  );
};

export default React.memo(ArticlesTable);
