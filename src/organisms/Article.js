import { Card, Button } from "react-bootstrap";

const Article = ({ article, onClose }) => {
  return (
    <>
      <Card.Header>{article.title}</Card.Header>
      <Card.Body>
        <Card.Title>{article.title}</Card.Title>
        <Card.Text>{article.description}</Card.Text>
        <Card.Text>{article.publish_date}</Card.Text>
      </Card.Body>
      <Card.Footer className="text-end">
        <Button onClick={onClose} variant="danger">
          Close
        </Button>
      </Card.Footer>
    </>
  );
};

export default Article;
