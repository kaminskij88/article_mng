import { createSlice } from "@reduxjs/toolkit";

const initialAlertState = { alerts: [] };

const alertSlice = createSlice({
  name: "alert",
  initialState: initialAlertState,
  reducers: {
    addAlert(state, action) {
      state.alerts.push(action.payload);
    },
    removeAlert(state, action) {
      state.alerts.filter((alert) => alert.id !== action.payload.id);
    },
  },
});

export const { addAlert, removeAlert } = alertSlice.actions;

export default alertSlice.reducer;
