import Backdrop from "../../atoms/Backdrop";
import { render } from "@testing-library/react";

describe("Atom:Backdrop", () => {
  test("should render backdrop", async () => {
    const mockFn = jest.fn();

    const { getByTestId } = render(<Backdrop onClick={mockFn} />);
    // eslint-disable-next-line jest/valid-expect
    expect(getByTestId("backdrop"));
  });
});
