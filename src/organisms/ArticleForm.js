import { useEffect, useState } from "react";
import { createOrUpdateArticle } from "../api/articleApi";
import { useForm } from "react-hook-form";
import "./ArticleForm.scss";
import { Container, Row, Button, Spinner } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { addAlert } from "../store/alertSlice";

const ArticleForm = ({ article, onClose }) => {
  const dispatch = useDispatch();

  const {
    register,
    handleSubmit,
    reset,
    setValue,
    formState: { errors, isSubmitSuccessful },
  } = useForm();

  const [spinner, setSpinner] = useState(false);

  useEffect(() => {
    if (article) {
      setValue("title", article.value.title);
      setValue("description", article.value.description);
      setValue("publish_date", article.value.publish_date);
    }
  }, [article, setValue]);

  const onSubmit = (data) => {
    setSpinner(true);
    let articleId = "";
    if (article) {
      articleId = article.id;
    }
    createOrUpdateArticle(data, articleId)
      .then(() => {
        setSpinner(false);
        article && onClose();
        dispatch(
          addAlert({
            id: new Date().getTime(),
            type: "success",
            message: article ? "Article edited" : "Article Addded",
          })
        );
      })
      .catch((error) => {
        setSpinner(false);
        dispatch(
          addAlert({
            id: new Date().getTime(),
            type: "danger",
            message: "Something went wrong",
          })
        );
      });
  };

  useEffect(() => {
    if (isSubmitSuccessful) {
      if (!article) {
        reset();
      }
    }
  }, [isSubmitSuccessful, reset, setSpinner, article]);

  return (
    <Container className="form-container">
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <label className="label" htmlFor="title">
            Title
          </label>
          <input
            {...register("title", {
              required: "This field is required",
              maxLength: { value: 35, message: "Title is too long" },
            })}
            id="title"
            className="text-input"
            placeholder="Title"
          />
          {errors.title && (
            <span className="error-message">{errors.title.message}</span>
          )}
        </Row>
        <Row>
          <label className="label" htmlFor="description">
            Description
          </label>
          <textarea
            {...register("description", {
              required: "This field is required",
            })}
            id="description"
            className="text-input"
            placeholder="Description"
          ></textarea>
          {errors.description && (
            <span className="error-message">{errors.description.message}</span>
          )}
        </Row>
        <Row>
          <label className="label" htmlFor="publish_date">
            Publish Date
          </label>
          <input
            {...register("publish_date", {
              required: "This field is required",
            })}
            id="publish_date"
            type="date"
            name="publish_date"
          ></input>
        </Row>
        <Row>
          <Button className="submit-button" type="submit" variant="success">
            {spinner ? (
              <Spinner
                as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true"
              />
            ) : (
              <span>{article ? "Edit" : "Submit"}</span>
            )}
          </Button>
          {article && (
            <Button variant="danger" onClick={onClose}>
              Close
            </Button>
          )}
        </Row>
      </form>
    </Container>
  );
};

export default ArticleForm;
