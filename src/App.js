import React, { Fragment } from "react";
import { Route, Switch } from "react-router-dom";
import Navbar from "../src/organisms/Navbar";
import AddArticle from "../src/pages/AddArticle";
import Articles from "../src/pages/Articles";
import "./App.scss";

const App = () => {
  return (
    <Fragment>
      <Navbar />
      <Switch>
        <Route path="/" exact>
          <Articles />
        </Route>
        <Route path="/add_article/:articleId?">
          <AddArticle />
        </Route>
      </Switch>
    </Fragment>
  );
};

export default App;
