import "./Modal.scss";
import { Fragment } from "react";
import ReactDOM from "react-dom";
import { Card } from "react-bootstrap";
import Backdrop from "../atoms/Backdrop";

const ModalOverlay = (props) => {
  return <Card className="modal-content">{props.children}</Card>;
};

const portalElement = document.getElementById("overlayes");
const Modal = (props) => {
  return (
    <Fragment>
      {ReactDOM.createPortal(
        <Backdrop onClose={props.onClose} />,
        portalElement
      )}
      {ReactDOM.createPortal(
        <ModalOverlay>{props.children}</ModalOverlay>,
        portalElement
      )}
    </Fragment>
  );
};

export default Modal;
