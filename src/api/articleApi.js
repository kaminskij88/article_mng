import axiosInstance from "./axios";

export const getArticles = () => {
  return axiosInstance.get("/articles.json");
};

export const createOrUpdateArticle = (body, id) => {
  return id
    ? axiosInstance.put(`/articles/${id}.json`, body)
    : axiosInstance.post("/articles.json", body);
};

export const deleteArticle = (id) => {
  return axiosInstance.delete(`/articles/${id}.json`);
};

export const getArticle = (id) => {
  return axiosInstance.get(`/articles/${id}.json`);
};
