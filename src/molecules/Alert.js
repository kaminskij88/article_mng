import { useSelector } from "react-redux";
import ReactDOM from "react-dom";
import AlertBody from "../molecules/AlertBody";

const Alert = () => {
  const portalElement = document.getElementById("overlayes");

  const alerts = useSelector((state) => state.alerts.alerts);

  return (
    <>
      {alerts &&
        alerts.map((alert) =>
          ReactDOM.createPortal(
            <AlertBody data-testid="alert" alert={alert} />,
            portalElement
          )
        )}
    </>
  );
};

export default Alert;
