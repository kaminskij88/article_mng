import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import App from "./App";
import Alert from "./molecules/Alert";
import "./index.css";
import store from "./store/store";

ReactDOM.render(
  <Fragment>
    <BrowserRouter>
      <Provider store={store}>
        <Alert />
        <App />
      </Provider>
    </BrowserRouter>
  </Fragment>,

  document.getElementById("root")
);
