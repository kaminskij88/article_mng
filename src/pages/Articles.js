import React from "react";
import { useEffect, useState, useCallback } from "react";
import { getArticles, deleteArticle, getArticle } from "../api/articleApi";
import { Container, Col, Row } from "react-bootstrap";
import ArticlesTable from "../organisms/ArticlesTable";
import Article from "../organisms/Article";
import Modal from "../molecules/Modal";
import { useDispatch } from "react-redux";
import { addAlert } from "../store/alertSlice";
import ArticleForm from "../organisms/ArticleForm";

const Articles = () => {
  const [articles, setArticles] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [article, setArticle] = useState([]);
  const [editArticleForm, setEditArticleForm] = useState(false);
  const [articleDataForEdit, setArticleDataForEdit] = useState([]);

  const dispatch = useDispatch();

  const fetchArticlesHandler = useCallback(() => {
    setIsLoading(true);
    getArticles()
      .then((result) => {
        const fetchedArticles = [];
        result &&
          Object.entries(result).forEach((article) => {
            fetchedArticles.push({ key: article[0], value: article[1] });
          });
        setArticles(fetchedArticles);
        setIsLoading(false);
      })
      .catch((error) => {
        dispatch(
          addAlert({
            id: new Date().getTime(),
            type: "danger",
            message: "Something went wrong we were unable to fetch articles",
          })
        );
        setIsLoading(false);
      });
  }, [setIsLoading, setArticles, dispatch]);

  useEffect(() => {
    fetchArticlesHandler();
  }, [fetchArticlesHandler]);

  const deleteRecord = (id) => {
    deleteArticle(id)
      .then((result) => {
        fetchArticlesHandler();
        dispatch(
          addAlert({
            id: new Date().getTime(),
            type: "info",
            message: "Article removed",
          })
        );
      })
      .catch(() => {
        dispatch(
          addAlert({
            id: new Date().getTime(),
            type: "danger",
            message: "You were unable to remove article",
          })
        );
      });
  };

  const showArticle = (id) => {
    getArticle(id)
      .then((result) => {
        setArticle(result);
        setOpenModal(true);
      })
      .catch((error) => console.log(error));
  };

  const hideModal = () => {
    setOpenModal(false);
  };

  const editArticle = (data) => {
    setArticleDataForEdit(data);
    setEditArticleForm(true);
  };

  const hideEditArticle = () => {
    setEditArticleForm(false);
    fetchArticlesHandler();
  };

  return (
    <Container fluid>
      <Row>
        <Col>
          <ArticlesTable
            articles={articles}
            deleteHandler={deleteRecord}
            isLoading={isLoading}
            showHandler={showArticle}
            editHandler={editArticle}
          />
          {openModal && (
            <Modal onClose={hideModal}>
              <Article article={article} onClose={hideModal} />
            </Modal>
          )}
          {editArticleForm && (
            <Modal onClose={hideEditArticle}>
              <ArticleForm
                article={articleDataForEdit}
                onClose={hideEditArticle}
              />
            </Modal>
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default React.memo(Articles);
