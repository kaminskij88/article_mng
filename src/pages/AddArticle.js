import ArticleForm from "../organisms/ArticleForm";
import { Fragment } from "react";
import { Container, Row, Col } from "react-bootstrap";
import "../App.scss";
import { useParams } from "react-router-dom";

const AddArticle = () => {
  const { articleId } = useParams();

  return (
    <Fragment>
      <Container>
        <Row className="justify-content-md-center">
          <Col md="8">
            <ArticleForm id={articleId} />
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
};

export default AddArticle;
