import { Navbar, Container, Nav, Button } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import "../App.scss";

const NavBar = () => {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <NavLink to="/">
            <Navbar.Brand className="logo">Article Manager</Navbar.Brand>
          </NavLink>
          <Nav className="justify-content-end">
            <Nav.Item>
              <NavLink to="/add_article">
                <Button variant="outline-primary">Add New Article</Button>
              </NavLink>
            </Nav.Item>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
};

export default NavBar;
