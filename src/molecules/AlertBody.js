import { Alert, Button } from "react-bootstrap";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { removeAlert } from "../store/alertSlice";
import "./AlertBody.scss";

const AlertBody = ({ alert }) => {
  const [show, setShow] = useState(true);

  const dispatch = useDispatch();

  const removeAlertBody = (id) => {
    setShow(false);
    dispatch(removeAlert({ id: id }));
  };

  return (
    <Alert
      className="alert-container"
      show={show}
      key={alert.id}
      variant={alert.type}
    >
      {alert.message}
      <div className="d-flex justify-content-end">
        <Button
          variant={`outline-${alert.type}`}
          onClick={removeAlertBody.bind(null, alert.id)}
        >
          Close
        </Button>
      </div>
    </Alert>
  );
};

export default AlertBody;
